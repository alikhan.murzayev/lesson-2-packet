// v1.0.2

package lesson_2_packet

import "math"

/*
Concatenation:
Returns a concatenations of strings 's1' and 's2' and first symbol of 's1'
*/
func Concatenation(s1, s2 string) (string, string) {
	return s1 + s1, string(s1[0])
}

/*
QuadraticEquation:
Solves a quadratic equation of type:
a*x^2 + b*x + c = 0
Returns x1, x2
*/
func QuadraticEquation(a, b, c float64) (x1, x2 float64) {
	var D float64 = b*b - 4*a*c
	return (-b + math.Sqrt(D)) / 2 * a, (-b - math.Sqrt(D)) / 2 * a

}

/*
BitwiseShift:
Returns a bitwise shift of 'a' to the left and to the right by 'b' bits
*/
func BitwiseShift(a, b int64) (int64, int64) {
	return a << b, a >> b
}
